-- ProbablyEngine Rotations
-- Released under modified BSD, see attached LICENSE.

-- Functions that require OffSpring

local L = ProbablyEngine.locale.get

function ProbablyEngine.protected.WoWSX()

    if WOWSX_LOADED then
      
        function Cast(spell, target)
          if type(spell) == "number" then
            CastSpellByID(spell, target)
          else
            CastSpellByName(spell, target)
          end
        end
        
        function CastGround(spell, target)
          local stickyValue = GetCVar("deselectOnClick")
          SetCVar("deselectOnClick", "0")
          CameraOrSelectOrMoveStart(1)
          Cast(spell)
          CameraOrSelectOrMoveStop(1)
          SetCVar("deselectOnClick", "1")
          SetCVar("deselectOnClick", stickyValue)
        end
        
        function Macro(text)
          return RunMacroText(text)
        end
        
        function UseItem(name, target)
          return UseItemByName(name, target)
        end

        ProbablyEngine.protected.unlocked = true
        ProbablyEngine.protected.method = "wowsx"
        ProbablyEngine.timer.unregister('detectUnlock')
        ProbablyEngine.timer.unregister('detectWoWSX')
        ProbablyEngine.print(L('unlock_wowsx'))

    end

end